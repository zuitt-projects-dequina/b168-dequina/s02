<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        body {
            border: 1px solid black;
            margin-left: 10%;
            margin-right: 10%;
            padding-left: 1rem;
            padding-right: 1rem;
        }
    </style>
    <title>S02 Activity</title>
</head>
<body>
    <h1>Activity 1</h1>

        <?php divisiblesOfFive(); ?>

    <hr/>

    <h1>Activity 2</h1>

        <?php array_push($student, 'John Smith') ?>
        <pre><?php print_r($student); ?></pre>
        <span>Number of Student/s: </span>
            <span><?php echo count($student); ?></span>
        
        <?php array_push($student, 'Jane Smith') ?>
        <pre><?php print_r($student); ?></pre>
        <span>Number of Student/s: </span>
            <span><?php echo count($student); ?></span>

        <?php array_shift($student) ?>
        <pre><?php print_r($student); ?></pre>
        <span>Number of Student/s: </span>
            <span><?php echo count($student); ?></span>
        
</body>
</html>