<?php

// Repetition Control Structures

/* 
    1. While Loop
    2. Do-While Loop
    3. For Loop
*/

// 1. While Loop

    function whileLoop() {
        $count = 5;

        while($count !== 0) {
            echo $count.'<br/>';
            $count--;
        }
    }

// 2. Do-While Loop

    function doWhileLoop() {
        $count = 20;

        do {
            echo $count.'<br/>';
            $count--;
        }
        while ($count >0);
    }

// 3. For Loop

    function forLoop(){
        for($count = 0; $count <= 20; $count++){
            echo $count.'<br/>';
        }
    }

// Continue and Break Statements

/* 
    Continue - is a keyword that allows the code to go to the next loop without finishing the current code block.

    Break - is a keyword that ends the execution of the current loop
*/

    function modifiedForLoop() {
        for ($count =0; $count <=20; $count++){
            if ($count % 2 === 0) {
                continue;
            }
            echo $count.'<br/>';
            if ($count > 10) {
                break;
            }
        }
    }

// Array Manipulation

// this array declaration is before version 5.4 of PHP
$studentNumbers = array(
    '2020-1923',
    '2020-1924',
    '2020-1925',
    '2020-1926',
    '2020-1927'
); 

// this array declaration was introduced in version 5.4 of PHP
$studentNumbers = [
    '2020-1923',
    '2020-1924',
    '2020-1925',
    '2020-1926',
    '2020-1927'
]; 

// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'HP', 'Toshiba', 'Fujitsu'];

$tasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

// Associative Array

$gradePeriods = [
    'firstGrading' => 98.5,
    'secondGrading' => 94.3,
    'thirdGrading' => 89.2,
    'fourthGrading' => 90.1
];

// Two-Dimensional Array

$heroes = [
    ['Iron man', 'Thor', 'Hulk'],
    ['Wolverine', 'Cyclops', 'Storm'],
    ['Darna', 'Captain Barbel', 'Lastikman']
];

// Two-Dimensional Array
$ironManPowers = [
    'regular' => ['repulsor blast', 'rocket punch'],
    'signature' => ['unibeam']
];

// Array Sorting

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

// Sort arrays

sort($sortedBrands); // to sort list in ascending order
rsort($sortedBrands); // to sort the list in desceding order

// Other Array Functions

    // In Array Function - serves as a search function of an element in an array
    function searchBrand($brands, $brand) {
        // in_array($theElementToBeSearch, $ArrayToBeSearch)
        return (in_array($brand, $brands)) ? 
                "$brand is in the array." 
                :
                "$brand is not in the array.";
    }

    // Array Reverse
    $reversedGradePeriods = array_reverse($gradePeriods);

    // for References: https://sabe.io/classes/php/arrays