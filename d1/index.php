<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>S02: Repitition Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Repetition Control Structures</h1>

    <h2>While Loop</h2>
        <?php whileLoop(); ?>
    
    <h2>Do-While Loop</h2>
        <?php doWhileLoop(); ?>
    
    <h2>For Loop</h2>
        <?php forLoop(); ?>
    
    <h2>Modified For Loop</h2>
        <?php modifiedForLoop(); ?>

    <h1>Array Manipulation</h1>

    <h2>Types of Arrays</h2>
    
        <h3>Simple Array</h3>
        <ul>
            <?php foreach($computerBrands as $brand) { ?> 
                <li><?php echo $brand ?></li>
            <?php } ?>
        </ul>


        <h3>Associative Array</h3>
        <ul>
            <!-- "< ?=" is just like < ?php echo-->
            <?php foreach($gradePeriods as $period => $grade) { ?>
                <li>Grade in <?= $period ?> is <?= $grade ?></li>
            <?php } ?>
        </ul>

        <h3>Multidimensional Array</h3>

        <ul>
            <?php
                foreach($heroes as $team) {
                    foreach($team as $member) {
                        ?>
                        <li><?php echo $member ?></li>
                    <?php }
                }
            ?>
        </ul>
                <!-- simpler approach from Angelo Zantua -->
        <ul>
        <?php foreach($heroes as $team){
            foreach($team as $member){
                echo '<li>'.$member.'</li>';
            }
        }?>
    </ul>

    <!-- Displaying a specific element in 2 Dimensional Array -->
    <h3>Accessing an specific Element</h3>
    <p>[2][2] <?php echo $heroes[2][2] ?></p>

    <!-- <pre> defines preformatted text. Text in a <pre> element is displayed in a fixed-width font, and the text preserves both spaces and line breaks. The text will be displayed exactly as written in the HTML source code -->

   <h2>Array Functions</h2>

        <h3>Sorting</h3>
        <!-- print_r is used to print or display information stored in a variable -->
        <pre><?php print_r($sortedBrands); ?></pre>

        <h3>Append</h3>
        <!-- syntax: array_push(arrayName, 'element') -->
        <!-- adding an element to the last part of the array -->
            <?php array_push($computerBrands, 'Apple') ?>
            <pre><?php print_r($computerBrands); ?></pre>

        <h3>Unshift</h3>
        <!-- syntax: array_unshift(arrayName, 'element') -->
        <!-- adding an element to the beggingn of the array -->
            <?php array_unshift($computerBrands, 'Dell'); ?>
            <pre><?php print_r($computerBrands); ?></pre>

        <h3>Remove</h3>
        <!-- syntax: array_pop(arrayName) -->
        <!-- deleting the last element of the array -->
            <?php array_pop($computerBrands); ?>
            <pre><?php print_r($computerBrands); ?></pre>

        <h3>Unshift</h3>
        <!-- syntax: array_shift(arrayName) -->
        <!-- deleting the first element of the array -->
            <?php array_shift($computerBrands); ?>
            <pre><?php print_r($computerBrands); ?></pre>

        <h3>Others</h3>
        <span>Count: </span>

            <span><?php echo count($computerBrands); ?></span>

        <!-- Acts like a search function in an array -->
        <h3>In Array</h3>
            <p><?php echo searchBrand($computerBrands, 'HP') ?></p>
            <p><?php echo searchBrand($computerBrands, 'Apple') ?></p>

        <h3>Reversed Grade Periods</h3>
            <pre><?php print_r($reversedGradePeriods); ?></pre>

        <!-- for references: https://sabe.io/classes/php/arrays -->
</body>
</html>